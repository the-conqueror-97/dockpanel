<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * This is for all cron jobs
 */
class CronController extends AbstractController
{

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    #[Route('/cron', name: 'app_cron')]
    public function index(Request $request): JsonResponse
    {
     
        return new JsonResponse([
            "success" => false,
        ]);
    }

    #[Route('/cron/users/rebuild-user-domains', name: 'app_cron_rebuild-users-domains')]
    public function rebuildUsers(Request $request): JsonResponse
    {
        $user = $this->userRepository->findAll();

        dd($user);

        return new JsonResponse([
            "success" => true,
        ]);
    }
}
