<?php

namespace App\Controller\Admin;

use App\Entity\Domain;
use App\Entity\RemoteServer;
use App\Repository\DomainRepository;
use App\Repository\RemoteServerRepository;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class RemoteServerCrudController extends AbstractCrudController
{

    private RemoteServerRepository $remoteServerRepository;
 

    public function __construct(RemoteServerRepository $remoteServerRepository)
    {
        $this->remoteServerRepository = $remoteServerRepository;
 
    }

    public static function getEntityFqcn(): string
    {
        return RemoteServer::class;
    }


    public function configureFields(string $pageName): iterable
    {

        $fields = [
            TextField::new('name', 'Server name'),
            TextField::new('server_ip', "IP"),

            TextField::new("userName", "User name"),
        ];

        if($pageName != "index") {
            $fields[] = TextField::new('password', "Password");
            $fields[] = TextareaField::new('ssh_key', "SSH key");


        }
      
        return $fields;
    }

}
