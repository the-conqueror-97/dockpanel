<?php

namespace App\Controller\Admin;

use App\Entity\Domain;
use App\Entity\RemoteServer;
use Doctrine\ORM\QueryBuilder;
use App\Repository\DomainRepository;
use App\Repository\RemoteServerRepository;
use Doctrine\Common\Collections\Criteria;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class DomainCrudController extends AbstractCrudController
{

    private RemoteServerRepository $remoteServerRepository;

    public function __construct(RemoteServerRepository $remoteServerRepository)
    {
        $this->remoteServerRepository = $remoteServerRepository;
    }


    public static function getEntityFqcn(): string
    {
        return Domain::class;
    }


    public function configureFields(string $pageName): iterable
    {
        $fields = [];

        switch ($pageName) {
            case "index":
                $fields[] = TextField::new('name');
                $fields[] = BooleanField::new('forceSslRedirect', "Force SSL");
                break;
            case "new":
                $fields[] = TextField::new('name');
                
                $fields[] = AssociationField::new('remoteServer', 'Remote servers (If more than 1 selected, it will work as HA)');

                $fields[] = BooleanField::new('forceSslRedirect', "Force SSL");
                $fields[] = TextareaField::new('sslCertificate');
                $fields[] = TextareaField::new('sslKey');
                $fields[] = TextareaField::new('sslCertificateAuthority');


                break;
            case "edit":
                $fields[] = AssociationField::new('remoteServer', 'Remote servers (If more than 1 selected, it will work as HA)');
                $fields[] = AssociationField::new('owner');
                $fields[] = BooleanField::new('forceSslRedirect', "Force SSL");

                break;
        }


        return $fields;
    }

    private function getServerChoices(): array
    {
        $servers = $this->remoteServerRepository->findAll();
      
        $list = [];

        foreach ($servers as $server) {
            $list["{$server->getName()} ({$server->getServerIp()})"] = $server->getId();
        }

        return $list;
    }
}
