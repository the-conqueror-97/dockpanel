<?php

namespace App\Controller;

use App\Repository\DomainRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * This is for all tasks to run when needed
 */
class TaskController extends AbstractController
{

    private DomainRepository $domainRepository;

    public function __construct(DomainRepository $domainRepository)
    {
        $this->domainRepository = $domainRepository;
    }


    #[Route('/task', name: 'app_task')]
    public function index(): Response
    {
        return $this->render('task/index.html.twig', [
            'controller_name' => 'TaskController',
        ]);
    }


    /**
     * 
     *
     * @return Response
     */
    #[Route('/task/rebuild-all-nginx', name: 'app_task-user')]
    public function buildUserNginxTemplates(): Response
    {

        $domains = $this->domainRepository->findAll();

        foreach ($domains as $domain) {
            // Here we have to generate right nginx configuration for this domain
        }


        return new JsonResponse([
            'success' => true,
        ]);
    }
}
