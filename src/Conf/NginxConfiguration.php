<?php

namespace App\Conf;

use App\Entity\Domain;

class NginxConfiguration
{

    private ?Domain $domain;

    public function __construct(?Domain $domain = null)
    {
        $this->domain = $domain;
    }

    public function builder()
    {

        if ($this->domain) {
            if ($this->domain->getForceSslRedirect() == true) {
                // Build force SSL redirect only
                $this->buildForceSSlRedirect();
            } else {

                if ($this->domain->getSslCertificate() != null) {
                    // SSL is enabled
                    $this->buildSsl();
                }

                // always build non SSL  
                $this->build();
            }
        }
    }

    /**
     * This function will build a redirection SSL
     *
     * @return void
     */
    public function buildForceSSlRedirect()
    {
    }

    /**
     * This function will build SSL Nginx configuration file
     *
     * @return void
     */
    public function buildSsl()
    {
    }

    /**
     * This function will build non SSL nginx configuration file
     *
     * @return void
     */
    public function build()
    {
    }
}
