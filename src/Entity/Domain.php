<?php

namespace App\Entity;

use App\Repository\DomainRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DomainRepository::class)]
class Domain
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $name;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    #[ORM\ManyToMany(targetEntity: RemoteServer::class, inversedBy: 'domains')]
    private $remoteServer;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'domains')]
    private $owner;

    #[ORM\Column(type: 'text', nullable: true)]
    private $sslCertificate;

    #[ORM\Column(type: 'text', nullable: true)]
    private $sslKey;

    #[ORM\Column(type: 'text', nullable: true)]
    private $sslCertificateAuthority;

    #[ORM\Column(type: 'boolean')]
    private $forceSslRedirect = false;

    public function __construct()
    {
        $this->remoteServer = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, RemoteServer>
     */
    public function getRemoteServer(): Collection
    {
        return $this->remoteServer;
    }

    public function addRemoteServer(RemoteServer $remoteServer): self
    {
        if (!$this->remoteServer->contains($remoteServer)) {
            $this->remoteServer[] = $remoteServer;
        }

        return $this;
    }

    public function removeRemoteServer(RemoteServer $remoteServer): self
    {
        $this->remoteServer->removeElement($remoteServer);

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getSslCertificate(): ?string
    {
        return $this->sslCertificate;
    }

    public function setSslCertificate(?string $sslCertificate): self
    {
        $this->sslCertificate = $sslCertificate;

        return $this;
    }

    public function getSslKey(): ?string
    {
        return $this->sslKey;
    }

    public function setSslKey(?string $sslKey): self
    {
        $this->sslKey = $sslKey;

        return $this;
    }

    public function getSslCertificateAuthority(): ?string
    {
        return $this->sslCertificateAuthority;
    }

    public function setSslCertificateAuthority(?string $sslCertificateAuthority): self
    {
        $this->sslCertificateAuthority = $sslCertificateAuthority;

        return $this;
    }

    public function getForceSslRedirect(): ?bool
    {
        return $this->forceSslRedirect;
    }

    public function setForceSslRedirect(bool $forceSslRedirect): self
    {
        $this->forceSslRedirect = $forceSslRedirect;

        return $this;
    }
}
