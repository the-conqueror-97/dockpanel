<?php

namespace App\Utility;

use App\Entity\User;

class SshKeyGenUtility
{


    private $rsaKey;

    private $privateKey;
    private $publicKey;


    private $pem;

    public function generate()
    {
        $this->rsaKey = openssl_pkey_new([
            'private_key_bits' => 1024,
            'private_key_type' => OPENSSL_KEYTYPE_RSA
        ]);

        $this->privateKey = openssl_pkey_get_private($this->rsaKey);
        openssl_pkey_export($this->privateKey, $this->pem); //Private Key

        $this->publicKey = $this->sshEncodePublicKey($this->rsaKey); //Public Key



    }

    /**
     * This will save the generated .ssh key in the right directory
     *
     * @param User|null $user
     * @param string $sshFileName
     * @return void
     */
    public function save(?User $user, string $sshFileName)
    {
        $umask = umask(0066);
        if ($user) {
            $sshDir = "/home/" + $user->getUsername() + "/.ssh/" + $sshFileName;
        } else {
            $sshDir = "~/.ssh/" + $sshFileName;
        }

        file_put_contents("$sshDir.rsa", $this->pem); //save private key into file
        file_put_contents("$sshDir.rsa.pub", $this->publicKey); //save public key into file
    }


    private function sshEncodePublicKey($privKey)
    {
        $keyInfo = openssl_pkey_get_details($privKey);
        $buffer  = pack("N", 7) . "ssh-rsa" .
            $this->sshEncodeBuffer($keyInfo['rsa']['e']) .
            $this->sshEncodeBuffer($keyInfo['rsa']['n']);
        return "ssh-rsa " . base64_encode($buffer);
    }
    private function sshEncodeBuffer($buffer)
    {
        $len = strlen($buffer);
        if (ord($buffer[0]) & 0x80) {
            $len++;
            $buffer = "\x00" . $buffer;
        }
        return pack("Na*", $len, $buffer);
    }
}
