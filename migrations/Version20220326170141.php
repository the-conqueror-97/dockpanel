<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220326170141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE domain (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domain_remote_server (domain_id INT NOT NULL, remote_server_id INT NOT NULL, INDEX IDX_46673171115F0EE5 (domain_id), INDEX IDX_4667317167FD7973 (remote_server_id), PRIMARY KEY(domain_id, remote_server_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE domain_remote_server ADD CONSTRAINT FK_46673171115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_remote_server ADD CONSTRAINT FK_4667317167FD7973 FOREIGN KEY (remote_server_id) REFERENCES remote_server (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain_remote_server DROP FOREIGN KEY FK_46673171115F0EE5');
        $this->addSql('DROP TABLE domain');
        $this->addSql('DROP TABLE domain_remote_server');
    }
}
